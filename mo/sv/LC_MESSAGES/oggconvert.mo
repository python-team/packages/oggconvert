��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J      �  	   	  
   	     )	     5	     =	     E	  "   N	     q	     �	     �	     �	  	   �	  
   �	     �	  %   �	  	   
     
     
     #
  A   B
     �
  
   �
     �
     �
  (   �
     �
     �
  
                  7     I     W  R   c     �  
   �     �     �  E   �     0     7  �   C     �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-13 22:08+0000
Last-Translator: Daniel Nylander <yeager@ubuntu.com>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% färdigt, ungefär %skvar %i timmar %i minuter %i sekunder 1 timme 1 minut 1 sekund <b><big>Konverterar fil
</big></b> <b>Konvertering</b> <b>Mål</b> <b>Källa</b> <i>Konverterar "%s"</i> Avancerat Alla filer Alla mediafiler Är du säker på att du vill stoppa? Ljudfiler Ljudkvalitet: Hej då! Kan inte spara till denna mapp Välj ett annat namn för filen, eller spara till en annan plats. Kodningen är färdig Filformat: Filnamn: Filen sparades som "%s". GStreamer-fel: förläsning misslyckades Ogg
Matroska Ogg:
Matroska: OggConvert Pausad Pausad (%.1f%% färdigt) Mapp att spara i: Välj en mapp Startar upp Filen finns redan i "%s". Om den ersätts kommer dess innehåll att skrivas över. Theora
Dirac Videofiler Videoformat: Videokvalitet: Din mediafil konverteras nu till Ogg-formatet. Detta kan ta lite tid. _Pausa _Återuppta Launchpad Contributions:
  Daniel Nylander https://launchpad.net/~yeager
  Zirro https://launchpad.net/~magne-andersson
  nicke https://launchpad.net/~niklas-aronsson okänd tid  