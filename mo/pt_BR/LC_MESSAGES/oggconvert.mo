��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  )   	     ;	  
   D	     O	     [	     b	  	   k	  &   u	     �	     �	     �	     �	  	   �	     �	     �	  6   
     Q
     d
     y
  $   �
  W   �
               .     ?  !   U     w     �  
   �     �     �     �     �  	   �  B   �     )     6     I     \  b   q     �  
   �  z  �     b                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-13 22:18+0000
Last-Translator: Heitor Thury Barreiros Barbosa <hthury@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% concluídos, cerca de %s restantes %i horas %i minutos %i segundos 1 hora 1 minuto 1 segundo <b><big>Convertendo arquivo
</big></b> <b>Conversão</b> <b>Destino</b> <b>Fonte</b> <i>Convertendo "%s"</i> Avançado Todos os arquivos Todos os arquivos de mídia Tem certeza de que deseja interromper esta operação? Arquivos de áudio Qualidade do áudio: Tchau então! Não é possível salvar nesta pasta Escolha um nome diferente para o arquivo a ser salvo, ou salve-o em um local diferente. Conversão concluída Formato do arquivo: Nome do arquivo: Arquivo salvo em "%s" Erro no GStreamer: preroll falhou Ogg
Matroska Ogg:
Matroska: OggConvert Parado Pausado (%.1f%% concluído) Pasta: Selecione uma pasta Iniciando O arquivo já existe em "%s". Esta operação irá sobrescreve-lo. Theora
Dirac Arquivos de vídeo Formato do vídeo: Qualidade do vídeo: O seu arquivo de mídia está sendo convertido para o formato Ogg. Isto pode levar bastante tempo. _Pausar _Continuar Launchpad Contributions:
  Alexandre Sapata Carbonell https://launchpad.net/~alexandre-sapata-carbonell
  Carlos Eduardo Moreira dos Santos https://launchpad.net/~cemsbr
  Gabriel Z M Ramos https://launchpad.net/~gabrielzmr
  Heitor Thury Barreiros Barbosa https://launchpad.net/~hthury
  Ricardo https://launchpad.net/~ricardofantin
  elias https://launchpad.net/~eliasalves123 tempo desconhecido  