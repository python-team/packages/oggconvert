��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  )   �     	  
   	     	     +	     1	  	   :	  %   D	     j	  
   {	     �	     �	     �	     �	     �	  $   �	     �	     
  	   
     )
  U   I
     �
     �
     �
     �
     �
               $  
   2     =     [     i  	   y  L   �     �     �     �     �  ^   
     i  
   p  �   {                      (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2010-05-03 16:29+0000
Last-Translator: Arnout Lok <Unknown>
Language-Team: Dutch <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% Afgewerkt.  Nog ongeveer %ste gaan %i uren %i minuten %i sekonden 1 uur 1 minuut 1 sekonde <b><big>Converteer Bestand
</big></b> <b>Conversie</b> Bestemming <b>Bron</b> <i>Converteer "%s"</i> Geavanceerd Alle Bestanden Alle Media Bestanden Bent u zeker dat u wenst te stoppen? Geluidsbestanden Audio Kwaliteit Tot ziens Kan niet bewaren in deze folder Kies een andere naam voor het te bewaren bestand, of bewaar het op een andere locatie Codering kompleet Bestandsformaat Bestandsnaam Bestand bewaard naar "%s" GStreamer fout: preroll gefaald Ogg
Matroska Ogg:
Matroska: Ogg Conversie Gepauzeerd Gepauzeerd (%.1f%% afgewerkt) Bewaar Folder Kies Een Folder Opstarten Het bestand bestaat reeds in "%s". Vervangen zal oude bestand overschrijven. Theora
Dirac Videobestanden Video Formaat Video Kwaliteit Het converteren van uw media bestand naar het Ogg formaat is bezig.  Dit kan enige tijd duren. _Pauze _Hervatten Launchpad Contributions:
  Arnout Lok https://launchpad.net/~arnout-lok
  Johny Provoost https://launchpad.net/~jepe
  arisos https://launchpad.net/~tomahawkh onbekende tijd  