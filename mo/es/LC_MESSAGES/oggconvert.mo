��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  *   �     	  
   '	     2	     >	     E	     N	  '   \	     �	     �	     �	     �	     �	     �	     �	  #   
     +
     =
     O
  #   ^
  W   �
     �
     �
            6   4     k     x  
   �     �     �     �     �  	   �  G   �     1     >     P     c  [   u     �  	   �  S  �     7                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2010-02-09 18:52+0000
Last-Translator: Gabriel <galvarez_mdp@yahoo.com.ar>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% completado, falta %saproximadamente %i horas %i minutos %i segundos 1 hora 1 minuto _n: 1 segundo <b><big>Convirtiendo archivo
</big></b> <b>Conversión</b> <b>Destino</b> <b>Origen</b> <i>Convirtiendo "%s"</i> Avanzado Todos los archivos Todos los archivos multimedia ¿Está seguro que quiere terminar? Archivos de audio Calidad de audio: ¡Hasta luego! No se puede guardar en esta carpeta Elija un nombre diferente para el archivo a guardar, o guárdelo en un lugar diferente. Codificación completa Formato del archivo: Nombre del archivo: Archivo guardado en "%s". Error en GStreamer: falló la carga del buffer inicial Ogg
Matroska Ogg:
Matroska: OggConvert Pausado Pausado (%.1f%% completado) Guardar Carpeta: Seleccionar una Carpeta Iniciando El archivo ya existe en "%s". Reemplazarlo sobrescribirá su contenido. Theora
Dirac Archivos de video Formato de vídeo: Calidad de video: Su archivo multimedia está siendo convertido a formato Ogg. Esto puede tomar mucho tiempo. _Pausar _Reanudar Launchpad Contributions:
  Cristian OLATE OPAZO https://launchpad.net/~cristian-olate
  Dante Díaz https://launchpad.net/~dante
  Felipe Lerena https://launchpad.net/~lipe
  Felipe Venegas https://launchpad.net/~fantomzz
  Guillermo Lo Coco https://launchpad.net/~glococo
  JorSol https://launchpad.net/~jorsol
  Leonardo Gastón De Luca https://launchpad.net/~leorockway
  Manuel Kaufmann https://launchpad.net/~humitos
  astiam https://launchpad.net/~astiam
  felixion https://launchpad.net/~felixion
  jeann https://launchpad.net/~jeancarlosn
  skarevoluti https://launchpad.net/~skarevoluti tiempo desconocido  