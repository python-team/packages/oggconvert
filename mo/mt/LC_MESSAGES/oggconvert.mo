��    )      d  ;   �      �     �     �  
   �  
   �     �     �     �  "   �          %     8     F     ]  	   f     p     �     �  	   �     �  K   �          -  
   :     E     Y     y     �  
   �     �     �  J   �               !     /  V   >     �     �     �     �  �  �     d     �  	   �  
   �     �     �     �  (   �     �     �     	      	     8	     A	     Q	     g	     y	  
   �	  &   �	  N   �	     
     "
     3
  !   A
  %   c
     �
     �
  
   �
     �
     �
  E   �
          !     1     C  U   V     �     �  B   �     �     	   &                !                          "   '                 %                                                                 $           
                #   (                )                  %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Save Folder: Select A Folder The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-17 23:48+0000
Last-Translator: David <borg.db@gmail.com>
Language-Team: Maltese <mt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% lest, fadal madwar %s %i siegħat %i minuti %i sekondi siegħa minuta sekonda <b><big>Konverżjoni tal-fajl
</big></b> <b>Konverżjoni</b> <b>Destinazzjoni</b> <b>Sors</b> <i>"%s" qed jiġi kkonvertit</i> Avvanzat Il-fajls kollha Ċert li trid tieqaf? Fajls ta' l-Awdjo Kwalità ta' l-Awdjo Ċaw mela! Ma tistax tissejvja ġo dan il-fowlder Agħżel isem ieħor għall-fajl li se tissejvja, jew issejvjaħ f'post ieħor Konverżjoni lesta Format tal-Fajl: Isem il-Fajl: Il-fajl ġie ssejvjat hawn: "%s". Żball ta' GStreamer: 'preroll' falla Ogg
Matroska Ogg:
Matroska: OggConvert Issejva ġo: Agħżel Fowlder Il-fajl diġà jeżisti ġo "%s". Jekk tibdlu, tħassarlu l-kontenut. Theora
Dirac Fajls tal-Video Format tal-Video: Kwalità tal-Video Il-fajl qed jiġi kkonvertit għall-format Ogg. Il-proċess ista' jieħu ħafna ħin. _Pawża _Kompli Launchpad Contributions:
  David https://launchpad.net/~borg-david ħin mhux magħruf  