��    ,      |  ;   �      �     �     �  
   �  
   �                 "         C     U     h     v     �  	   �     �     �     �     �  	   �     �  K        [     m  
   z     �     �     �  
   �     �     �     �     �     �  J   
     U     b     n     |  V   �     �     �     �       �    '   �  	   �     �  	   �  	   �     �  	   	  &   	     7	     H	     ]	  #   u	     �	     �	     �	     �	     �	     �	     
  "   
  @   B
     �
     �
     �
     �
     �
     �
  #   �
  
             6     ?     O  E   \     �     �     �     �  \   �  
   7     B  �   J          	   )             !   $               '          %   *                  (   "                                            #                          
                &   +                ,                  %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2010-04-24 09:36+0000
Last-Translator: Piotr Sokół <Unknown>
Language-Team: Polish <pl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% ukończono, około %s pozostało %i godzin %i minut %i sekund 1 godzina 1 minuta 1 sekunda <b><big>Konwertowanie pliku
</big></b> <b>Konwersja</b> <b>Plik docelowy</b> <b>Plik źródłowy</b> <i>Konwertowanie pliku „%s”</i> Zaawansowane Wszystkie pliki Wszystkie pliki multimedialne Zatrzymać kodowanie? Pliki dźwiękowe Jakość dźwięku: Do zobaczenia! Nie można zapisać w tym katalogu Proszę wybrać inną nazwę lub położenie zapisywanego pliku. Zakończono kodowanie Format pliku: Nazwa pliku: Zapisano plik „%s”. Ogg
Matroska Ogg:
Matroska: Konwerter multimediów OggConverter Wstrzymano Wstrzymano (%.1f%% ukończone) Katalog: Wybór katalogu Uruchamianie Plik już istnieje w "%s". Zastąpienie go nadpisze jego zawartość. Theora
Dirac Pliki wideo Format wideo: Jakość obrazu: Plik multimedialny jest w tej chwili konwertowany do formatu Ogg. Może to chwilę potrwać. _Wstrzymaj _Wznów Launchpad Contributions:
  Brech https://launchpad.net/~brecho
  Maciej Habant https://launchpad.net/~lays
  Piotr Sokół https://launchpad.net/~piotr-sokol
  pp/bs https://launchpad.net/~pawprok nieznany czas  